import 'dart:math';

class CalculatorBrain {
  CalculatorBrain({this.height, this.weight});

  final int height;
  final int weight;

  double _bmi;

  String calculateBMI() {
    _bmi = weight / pow(height / 100, 2);
    return _bmi.toStringAsFixed(1);
  }

  String getResult() {
    if (_bmi >= 25) {
      return 'Sobrepeso';
    } else if (_bmi > 18.5) {
      return 'Normal';
    } else {
      return 'Poco peso';
    }
  }

  String getInterpretation() {
    if (_bmi >= 25) {
      return 'Estás bien gordito, bájale un poco a la comida xfas';
    } else if (_bmi > 18.5) {
      return 'Estás bien mi rey';
    } else {
      return 'Deberías comer más queso con arroz ;)';
    }
  }
}
